var noop = function () {

};

var isUndef = function (data) {
  return !data && data !== false;
};

var isEmpty = function (obj) {
  return !obj || (Object.keys(obj).length === 0 && obj.constructor === Object);
};

var extend = function (defaults, options) {
  var extended = {};
  var prop;
  for (prop in defaults) {
    if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
      extended[prop] = defaults[prop];
    }
  }
  for (prop in options) {
    if (Object.prototype.hasOwnProperty.call(options, prop)) {
      extended[prop] = options[prop];
    }
  }
  return extended;
};

var findAppKey = function (appKey) {
  var storageType;
  if (root.sessionStorage[appKey] && root.localStorage[appKey]) {
    throw new SpecifyStorageTypeError();
  } else if (root.localStorage[appKey]) {
    storageType = 'local';
  } else if (root.sessionStorage[appKey]) {
    storageType = 'session';
  } else {
    throw new UndefinedAppKeyError();
  }
  return storageType;
};

var get
