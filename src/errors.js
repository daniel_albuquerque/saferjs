var ModifiedStorageError = function () {
  this.name = 'ModifiedStorageError';
  this.message = 'Encrypted storage was modified outside Safer.';
  this.stack = (new Error()).stack;
};
ModifiedStorageError.prototype = Object.create(Error.prototype);
ModifiedStorageError.prototype.constructor = ModifiedStorageError;

var SpecifyStorageTypeError = function () {
  this.name = 'SpecifyStorageTypeError';
  this.message = 'This appKey exists in local and session storage. Specify the one you want to retrieve.';
  this.stack = (new Error()).stack;
};
SpecifyStorageTypeError.prototype = Object.create(Error.prototype);
SpecifyStorageTypeError.prototype.constructor = SpecifyStorageTypeError;

var UndefinedAppKeyError = function () {
  this.name = 'UndefinedAppKeyError';
  this.message = 'This appKey does not exist.';
  this.stack = (new Error()).stack;
};
UndefinedAppKeyError.prototype = Object.create(Error.prototype);
UndefinedAppKeyError.prototype.constructor = UndefinedAppKeyError;
