module.exports = function (grunt) {
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    concat: {
      options: {
        separator: ';\n\n'
      },
      default: {
        src: [
          'node_modules/crypto-js/crypto-js.js',
          'src/errors.js',
          'src/utils.js',
          'src/core.js'
        ],
        dest: '<%= pkg.dist %>/<%= pkg.name %>.js'
      }
    },

    uglify: {
      options: {
        preserveComments: false
      },
      default: {
        files: [
          {
            expand: true,
            cwd: '<%= pkg.dist %>/',
            src: ['*.js', '**/*.js'],
            dest: '<%= pkg.dist %>/',
            ext: '.min.js'
          }
        ]
      }
    },

    umd: {
      all: {
        options: {
          src: 'path/to/input.js',
          dest: 'path/to/output.js', // optional, if missing the src will be used

          // optional, a template from templates subdir
          // can be specified by name (e.g. 'umd'); if missing, the templates/umd.hbs
          // file will be used from [libumd](https://github.com/bebraw/libumd)
          template: 'path/to/template.hbs',

          objectToExport: 'library', // optional, internal object that will be exported
          amdModuleId: 'id', // optional, if missing the AMD module will be anonymous
          globalAlias: 'alias', // optional, changes the name of the global variable

          deps: { // optional, `default` is used as a fallback for rest!
            'default': ['foo', 'bar'],
            amd: ['foobar', 'barbar'],
            cjs: ['foo', 'barbar'],
            global: ['foobar', {depName: 'param'}]
          }
        }
      }
    },

    clean: {
      build: [
        '<%= pkg.dist %>/sandBox/build.txt',
        '<%= pkg.dist %>/js/stores'
      ]
    },

    eslint: {
      options: {
        config: '.eslintrc.js',
        reset: true
      },
      target: ['src/**/*.js']
    },

    mochaTest: {
      test: {
        options: {
          reporter: 'spec',
          captureFile: 'testResult.txt'
        },
        src: ['test/*.js']
      }
    },

    jsdoc: {
      dist: {
        src: ['src/core.js'],
        options: {
          destination: 'doc'
        }
      }
    }

  });

  require('load-grunt-tasks')(grunt);

  /* For session */
  grunt.registerTask('prod', ['concat', 'uglify']);
  grunt.registerTask('dev', ['concat:dev']);
  grunt.registerTask('default', ['concat', 'uglify', 'jsdoc']);
};
