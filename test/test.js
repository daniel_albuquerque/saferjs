var root = window;

describe('Safer storage', function () {
  describe('create an appKey', function () {
    beforeEach(function () {
      localStorage.removeItem('testAppKey');
      sessionStorage.removeItem('testAppKey');
    });
    it('should initialize and create an appKey in localStorage by default', function () {
      safer.create('testAppKey');
      expect(root.localStorage['testAppKey']).to.be.ok();
    });
    it('should initialize and create an appKey in localStorage', function () {
      safer.create('testAppKey', {
        'smartRecover': false,
        'storageType': 'local'
      });
      expect(root.localStorage['testAppKey']).to.be.ok();
    });
    it('should initialize and create an appKey in sessionStorage', function () {
      safer.create('testAppKey', {
        'smartRecover': false,
        'storageType': 'session'
      });
      expect(root.sessionStorage['testAppKey']).to.be.ok();
    });
  });
});
