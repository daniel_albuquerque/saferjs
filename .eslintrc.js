module.exports = {
  "extends": "standard",
  "plugins": [
    "standard",
    "promise"
  ],
  "env": {
    "amd": true,
    "browser": true,
    "node": true
  },
  "globals": {
    "CryptoJS": true,
    "root": true,
    "_": true
  },
  "rules": {
    "linebreak-style": ["error", "windows"],
    "semi": ["error", "always"],
    "no-undef": "warn",
    "no-unused-vars": "warn"
  }
};