module.exports = {
  "extends": "standard",
  "plugins": [
    "standard",
    "promise"
  ],
  "env": {
    "amd": true,
    "browser": true,
    "node": true
  },
  "globals": {
    "root": true,
    "_": true,
    "describe": true,
    "before": true,
    "after": true,
    "beforeEach": true,
    "afetrEach": true,
    "it": true,
    "safer": true,
    "expect": true
  },
  "rules": {
    "linebreak-style": ["error", "windows"],
    "semi": ["error", "always"]
  }
};