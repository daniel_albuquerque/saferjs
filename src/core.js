/**
 * @param appKey - The application key for local or session storage
 * @param create - Flag to determine if a new localstorage key will be created
 * @param password (optional) - Password for encryption
 * @constructor
 */
var Safer = function (appKey, isCreate, options, password) {
  var encryptedStorage, initData;
  this.appKey = appKey;
  this.password = password || 'password';
  this.storageType = options.storageType;
  if (isCreate) {
    initData = '{}';
    this.md5 = CryptoJS.MD5(initData);
    this.smartRecover = options.smartRecover;
    this.backupStorage = {};
    this._store(initData, true);
  } else {
    encryptedStorage = root[this.storageType + 'Storage'].getItem(this.appKey);
    this.md5 = encryptedStorage.slice(encryptedStorage.length - this.hex, encryptedStorage.length);
    this.smartRecover = isUndef(options.smartRecover) ? root['saferBackup'][this.storageType + 'Storage'].hasOwnProperty(this.appKey) : options.smartRecover;
    this.backupStorage = this.smartRecover ? root[this.storageType + 'Storage'].getItem(this.appKey) : {};
  }
};

Safer.prototype.hex = 32;

Safer.prototype._verifyMD5 = function (encryptedStorage) {
  return this.md5 ? _.isEqual(CryptoJS.MD5(encryptedStorage), this.md5) : true;
};

/**
 *
 * @param string - The string you want to store
 * @param encrypt - Flag to determine if string will be encrypted
 * @private
 */
Safer.prototype._store = function (string, encrypt) {
  var encryptedStorage;
  this.md5 = CryptoJS.MD5(string);
  encryptedStorage = encrypt ? CryptoJS.AES.encrypt(string, this.md5.toString()).toString() : string;
  encryptedStorage += this.md5;
  if (this.smartRecover) {
    root.saferBackup[this.appKey] = encryptedStorage;
    this.backupStorage = encryptedStorage;
  }
  root[this.storageType + 'Storage'].setItem(this.appKey, encryptedStorage);
};

Safer.prototype._backupAlignment = function () {
  return root.saferBackup[this.appKey];
};

Safer.prototype._getStorage = function () {
  return root[this.storageType + 'Storage'].getItem(this.appKey);
};

Safer.prototype._recoverBackupIfExists = function (encryptedStorage) {
  if (encryptedStorage !== this.backupStorage) {
    encryptedStorage = this.backupStorage;
    this._store(this.backupStorage, false);
  }
};

/**
 * Function to retireve the storage
 */
Safer.prototype.get = function (key) {
  var encryptedStorage, decryptedStorage, isValid, json;
  encryptedStorage = root[this.storageType + 'Storage'].getItem(this.appKey);
  encryptedStorage = encryptedStorage.slice(0, encryptedStorage.length - this.hex);
  decryptedStorage = CryptoJS.AES.decrypt(encryptedStorage, this.md5.toString());
  decryptedStorage = decryptedStorage.toString(CryptoJS.enc.Utf8);
  isValid = this._verifyMD5(decryptedStorage);
  // this._recoverBackupIfExists(encryptedStorage);
  if (isValid) {
    json = JSON.parse(decryptedStorage || '{}');
    return key ? json[key] : json;
  }
};

/**
 * Function to set a value in the storage
 * @param key
 * @param obj
 */
Safer.prototype.set = function (key, obj) {
  var parsedStorage = this.get();
  parsedStorage[key] = obj;
  var string = JSON.stringify(parsedStorage);
  this._store(string, true);
};

/* Exposed methods */

var safer = {};

safer.create = function (appKey, options, password) {
  var defaultOptions = {
    'smartRecover': false,
    'storageType': 'local'
  };
  options = extend(defaultOptions, options);
  return new Safer(appKey, true, options, password);
};

safer.retrieve = function (appKey, options, password) {
  try {
    options = extend({}, options);
    options.storageType = options.storageType ? options.storageType : findAppKey.call(this, appKey);
  } catch (err) {
    console.log(err);
    return undefined;
  }
  return new Safer(appKey, false, options);
};

safer.retrieveLocalStorage = function (appKey, options, password) {
  var defaultOptions = {
    'storageType': 'local'
  };
  options = extend(defaultOptions, options);
  safer.retrieve(appKey, options, password);
};

safer.retrieveSessionStorage = function (appKey, options, password) {
  var defaultOptions = {
    'storageType': 'session'
  };
  options = extend(defaultOptions, options);
  safer.retrieve(appKey, options, password);
};
